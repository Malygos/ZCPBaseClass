//
//  UIViewController+ZCPBase.m
//  ZCPKit
//
//  Created by zcp on 2019/1/7.
//  Copyright © 2019 zcp. All rights reserved.
//

#import "UIViewController+ZCPBase.h"
#import "Aspects.h"
#import <objc/runtime.h>
#import "ZCPGlobal.h"

@protocol __ZCPViewControllerBaseProtocol <NSObject>

@optional
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@property (nonatomic, strong) NSNumber *needsTapToDismissKeyboard;

- (void)registerKeyboardNotification;
- (void)breakdown;
- (void)dismissKeyboard;
- (void)keyboardWillShow:(NSNotification *)notification;
- (void)keyboardDidShow:(NSNotification *)notification;
- (void)keyboardWillHide:(NSNotification *)notification;
- (void)keyboardDidHide:(NSNotification *)notification;
- (void)keyboardWillChangeFrame:(NSNotification *)notification;
- (void)keyboardDidChangeFrame:(NSNotification *)notification;
- (void)selfViewTapped:(UITapGestureRecognizer *)tap;

- (BOOL)isHideLeftBarButton;
- (void)initNavigationBar;
- (void)backTo;
- (void)setBackBarButton;

@end

#pragma mark - UIViewController基础扩展类
@interface _UIViewControllerZCPBaseExtension : NSObject <__ZCPViewControllerBaseProtocol>
@property (nonatomic, weak) UIViewController *viewController;
@end

@interface _UIViewControllerZCPBaseExtension () <__ZCPViewControllerBaseProtocol>

@end

@implementation _UIViewControllerZCPBaseExtension

@synthesize tapGesture = _tapGesture;
@synthesize needsTapToDismissKeyboard = _needsTapToDismissKeyboard;

- (UITapGestureRecognizer *)tapGesture {
    if (_tapGesture == nil) {
        _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selfViewTapped:)];
        _tapGesture.cancelsTouchesInView = YES;
    }
    return _tapGesture;
}

#pragma mark - keyboard

- (void)registerKeyboardNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidChangeFrame:) name:UIKeyboardDidChangeFrameNotification object:nil];
}

- (void)breakdown {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidChangeFrameNotification object:nil];
}

- (void)dismissKeyboard {
    [self.viewController.view endEditing:YES];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    if ([self.needsTapToDismissKeyboard boolValue]) {
        [self.viewController.view addGestureRecognizer:self.tapGesture];
    }
}
- (void)keyboardDidShow:(NSNotification *)notification {}
- (void)keyboardWillHide:(NSNotification *)notification {
    if (self.tapGesture) {
        [self.viewController.view removeGestureRecognizer:self.tapGesture];
    }
}
- (void)keyboardDidHide:(NSNotification *)notification {}
- (void)keyboardWillChangeFrame:(NSNotification *)notification {}
- (void)keyboardDidChangeFrame:(NSNotification *)notification {}

- (void)selfViewTapped:(UITapGestureRecognizer *)tap {
    if (tap.view == self.viewController.view) {
        [self dismissKeyboard];
    }
}

#pragma mark - NavigationBar

- (BOOL)isHideLeftBarButton {
    return NO;
}

- (void)initNavigationBar {}

- (void)backTo {
    if ([self.viewController.navigationController respondsToSelector:@selector(popViewControllerAnimated:)]) {
        [self.viewController.navigationController popViewControllerAnimated:YES];
    }else if([self.viewController.tabBarController.navigationController respondsToSelector:@selector(popViewControllerAnimated:)]){
        [self.viewController.tabBarController.navigationController popViewControllerAnimated:YES];
    }else{
        [self.viewController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)setBackBarButton {}

@end


@interface UIViewController () <__ZCPViewControllerBaseProtocol>
@property (nonatomic, strong) _UIViewControllerZCPBaseExtension *zcpBaseExtension;
@end

@implementation UIViewController (ZCPBase)

#pragma mark - swizzing

+ (void)load {
    NSError * __strong error = nil;
    
    [UIViewController aspect_hookSelector:@selector(viewDidLoad) withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo> aspectInfo) {
        UIViewController *vc    = aspectInfo.instance;
        [vc zcp_viewDidLoad];
    } error:&error];

    if (error) NSAssert(YES, error.localizedDescription);

    [UIViewController aspect_hookSelector:@selector(viewWillAppear:) withOptions:AspectPositionAfter usingBlock:^(id<AspectInfo> aspectInfo) {
        UIViewController *vc    = aspectInfo.instance;
        NSArray *arguments      = aspectInfo.arguments;
        [vc zcp_viewWillAppear:[arguments[0] boolValue]];
    } error:&error];
    
    if (error) NSAssert(YES, error.localizedDescription);
}

- (void)zcp_viewDidLoad {
    SELF_CONFORMS_TO_PROTOCOL(@protocol(ZCPViewControllerBaseProtocol), ^() {
        self.view.backgroundColor = [UIColor whiteColor];
        // 键盘点击隐藏事件
        self.needsTapToDismissKeyboard = @YES;
        if (![self isHideLeftBarButton]) {
            [self setBackBarButton];
        }
        // 添加键盘响应事件
        [self registerKeyboardNotification];
    });
}

- (void)zcp_viewWillAppear:(BOOL)animated {
    SELF_CONFORMS_TO_PROTOCOL(@protocol(ZCPViewControllerBaseProtocol), ^() {
        [self.view endEditing:YES];
    });
}

#pragma mark - __ZCPViewControllerBaseProtocol

- (UITapGestureRecognizer *)tapGesture {
    return self.zcpBaseExtension.tapGesture;
}
- (void)setTapGesture:(UITapGestureRecognizer *)tapGesture {
    [self.zcpBaseExtension setTapGesture:tapGesture];
}
- (NSNumber *)needsTapToDismissKeyboard {
    return self.zcpBaseExtension.needsTapToDismissKeyboard;
}
- (void)setNeedsTapToDismissKeyboard:(NSNumber *)needsTapToDismissKeyboard {
    [self.zcpBaseExtension setNeedsTapToDismissKeyboard:needsTapToDismissKeyboard];
}
- (void)registerKeyboardNotification {
    return [self.zcpBaseExtension registerKeyboardNotification];
}
- (void)breakdown {
    return [self.zcpBaseExtension breakdown];
}
- (void)dismissKeyboard {
    return [self.zcpBaseExtension dismissKeyboard];
}
- (void)keyboardWillShow:(NSNotification *)notification {
    return [self.zcpBaseExtension keyboardWillShow:notification];
}
- (void)keyboardDidShow:(NSNotification *)notification {
    return [self.zcpBaseExtension keyboardDidShow:notification];
}
- (void)keyboardWillHide:(NSNotification *)notification {
    return [self.zcpBaseExtension keyboardWillHide:notification];
}
- (void)keyboardDidHide:(NSNotification *)notification {
    return [self.zcpBaseExtension keyboardDidHide:notification];
}
- (void)keyboardWillChangeFrame:(NSNotification *)notification {
    return [self.zcpBaseExtension keyboardWillChangeFrame:notification];
}
- (void)keyboardDidChangeFrame:(NSNotification *)notification {
    return [self.zcpBaseExtension keyboardDidChangeFrame:notification];
}
- (void)selfViewTapped:(UITapGestureRecognizer *)tap {
    return [self.zcpBaseExtension selfViewTapped:tap];
}
- (BOOL)isHideLeftBarButton {
    return [self.zcpBaseExtension isHideLeftBarButton];
}
- (void)initNavigationBar {
    return [self.zcpBaseExtension initNavigationBar];
}
- (void)backTo {
    return [self.zcpBaseExtension backTo];
}
- (void)setBackBarButton {
    return [self.zcpBaseExtension setBackBarButton];
}

#pragma mark - getters and setters

- (_UIViewControllerZCPBaseExtension *)zcpBaseExtension {
    _UIViewControllerZCPBaseExtension *extension = objc_getAssociatedObject(self, @selector(zcpBaseExtension));
    if (!extension) {
        extension = [[_UIViewControllerZCPBaseExtension alloc] init];
        extension.viewController = self;
        self.zcpBaseExtension = extension;
    }
    return extension;
}

- (void)setZcpBaseExtension:(_UIViewControllerZCPBaseExtension *)zcpBaseExtension {
    objc_setAssociatedObject(self, @selector(zcpBaseExtension), zcpBaseExtension, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
