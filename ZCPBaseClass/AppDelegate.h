//
//  AppDelegate.h
//  ZCPBaseClass
//
//  Created by zcp on 2019/5/16.
//  Copyright © 2019 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

